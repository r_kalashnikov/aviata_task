from asyncio import sleep
from fastapi import FastAPI
from fastapi.responses import FileResponse

from settings import PROVIDER_A_TIME_TO_SLEEP


provider_a_app = FastAPI()


@provider_a_app.post('/search')
async def search_flights_provider_a():
    await sleep(PROVIDER_A_TIME_TO_SLEEP)
    return FileResponse('response_a.json')

from asyncio import sleep
from fastapi import FastAPI
from fastapi.responses import FileResponse

from settings import PROVIDER_B_TIME_TO_SLEEP

provider_b_app = FastAPI()


@provider_b_app.post('/search')
async def search_flights_provider_b():
    await sleep(PROVIDER_B_TIME_TO_SLEEP)
    return FileResponse('response_b.json')

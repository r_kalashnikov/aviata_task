import asyncio
import os

from celery import Celery

from helpers.request_senders import post_to_providers

celery = Celery(__name__)
celery.conf.broker_url = os.environ.get('CELERY_BROKER_URL', 'redis://localhost:6379')
celery.conf.result_backend = os.environ.get('CELERY_RESULT_BACKEND', 'redis://localhost:6379')


@celery.task(name='create_task')
def create_task():
    res = post_to_providers()
    answer = asyncio.run(res)
    return answer

#
# @celery.task(name="create_today_currency")
# def create_everyday_rate():
#     providers_answer_coro = post_to_provider('http://airflow:9000/search', 80)
#     asyncio.run(providers_answer_coro)
#
#     currency_coro = create_today_currency()
#     asyncio.run(currency_coro)
#     return True
#
#
# celery.conf.beat_schedule = {
#     'task-name': {
#         'task': 'create_today_currency',
#         'schedule': crontab(hour='12',
#                             minute=0,
#                             ),
#     },
# }

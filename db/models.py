from tortoise import fields
from tortoise.models import Model


class Search(Model):
    id = fields.UUIDField(pk=True)
    created_at = fields.DateField(auto_now_add=True)
    content = fields.JSONField(null=False)

    class Meta:
        table = 'search'

    def __str__(self):
        return f'{self.id}'


class Rate(Model):
    id = fields.IntField(pk=True)
    created_at = fields.DateField(auto_now_add=True)
    AUD = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    AZN = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    AMD = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    BYN = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    BRL = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    HUF = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    HKD = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    GEL = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    DKK = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    AED = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    USD = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    EUR = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    INR = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    IRR = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    CAD = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    CNY = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    KWD = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    KGS = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    MYR = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    MXN = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    MDL = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    NOK = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    PLN = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    SAR = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    RUB = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    XDR = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    SGD = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    TJS = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    THB = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    TRY = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    UZS = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    UAH = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    GBP = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    CZK = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    SEK = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    CHF = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    ZAR = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    KRW = fields.DecimalField(decimal_places=2, max_digits=20, null=False)
    JPY = fields.DecimalField(decimal_places=2, max_digits=20, null=False)

    class Meta:
        table = 'rates'

    def __str__(self):
        return f'{self.created_at}'

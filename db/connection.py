import os

from tortoise.contrib.fastapi import register_tortoise


def run_db_for_app(app):
    register_tortoise(
        app,
        config={
            'connections': {
                'default': {
                    'engine': 'tortoise.backends.asyncpg',
                    'credentials': {
                        'host': os.getenv('HOST'),
                        'port': os.getenv('PORT'),
                        'user': os.getenv('USER'),
                        'password': os.getenv('PASSWORD'),
                        'database': os.getenv('DATABASE'),
                    }
                },
            },
            'apps': {
                'airflow': {
                    'models': ['db.models'],
                    'default_connection': 'default',
                }
            }
        },
        generate_schemas=True,
        add_exception_handlers=True,
    )

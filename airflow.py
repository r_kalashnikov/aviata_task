from datetime import datetime

from tortoise.exceptions import DoesNotExist

from db.connection import run_db_for_app
from celery.result import AsyncResult
from fastapi import FastAPI
from starlette.responses import JSONResponse

from db.models import Search
from helpers.builders import build_search_result, create_today_currency
from helpers.validators import is_valid_currency
from worker import create_task


airflow_app = FastAPI()


@airflow_app.post('/search')
def search_flights():
    task = create_task.delay()
    return JSONResponse({'search_id': task.id})


@airflow_app.get('/result/{search_id}/{currency}')
async def get_answer(search_id, currency):

    if not is_valid_currency(currency):
        return JSONResponse({'Error': 'Currency is not valid!'})

    await create_today_currency()
    try:
        search = await Search.get(id=search_id)
        result = await build_search_result(search_id, 'SUCCESS', search.content, currency)

    except DoesNotExist:
        task_result = AsyncResult(search_id)
        result = await build_search_result(search_id, task_result.status, task_result.result, currency)

        if task_result.status == 'SUCCESS':
            await Search.create(id=search_id, created_at=datetime.today(), content=task_result.result)

    return JSONResponse(result)


run_db_for_app(airflow_app)

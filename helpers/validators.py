CURRENCIES: list = ['KZT', 'AUD', 'AZN', 'AMD', 'BYN', 'BRL', 'HUF', 'HKD', 'GEL', 'DKK', 'AED', 'USD', 'EUR', 'INR',
                    'IRR', 'CAD', 'CNY', 'KWD', 'KGS', 'MYR', 'MXN', 'MDL', 'NOK', 'PLN', 'SAR', 'RUB', 'XDR', 'SGD',
                    'TJS', 'THB', 'TRY', 'UZS', 'UAH', 'GBP', 'CZK', 'SEK', 'CHF', 'ZAR', 'KRW', 'JPY']


def is_valid_currency(currency: str) -> bool:
    return currency in CURRENCIES

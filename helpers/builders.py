from datetime import datetime
from tortoise.exceptions import DoesNotExist

from db.models import Rate
from helpers.parsers import add_price_and_sort_json_flight, get_current_rate


async def build_search_result(search_id: str, search_status: str, search_result: [dict], currency: str) -> dict:
    result = {
        'search_id': search_id,
        'status': 'COMPLETED' if search_status == 'SUCCESS' else 'PENDING',
        'items': await add_price_and_sort_json_flight(search_result, currency) if search_status == 'SUCCESS' else []
    }

    return result


async def create_today_currency() -> None:
    today_date = datetime.today()

    try:
        await Rate.get(created_at=today_date)

    except DoesNotExist:
        rate = get_current_rate()
        await Rate.create(created_at=today_date, AUD=rate['AUD'], AZN=rate['AZN'], AMD=rate['AMD'],
                          BYN=rate['BYN'], BRL=rate['BRL'], HUF=rate['HUF'], HKD=rate['HKD'],
                          GEL=rate['GEL'], DKK=rate['DKK'], AED=rate['AED'], USD=rate['USD'],
                          EUR=rate['EUR'], INR=rate['INR'], IRR=rate['IRR'], CAD=rate['CAD'],
                          CNY=rate['CNY'], KWD=rate['KWD'], KGS=rate['KGS'], MYR=rate['MYR'],
                          MXN=rate['MXN'], MDL=rate['MDL'], NOK=rate['NOK'], PLN=rate['PLN'],
                          SAR=rate['SAR'], RUB=rate['RUB'], XDR=rate['XDR'], SGD=rate['SGD'],
                          TJS=rate['TJS'], THB=rate['THB'], TRY=rate['TRY'], UZS=rate['UZS'],
                          UAH=rate['UAH'], GBP=rate['GBP'], CZK=rate['CZK'], SEK=rate['SEK'],
                          CHF=rate['CHF'], ZAR=rate['ZAR'], KRW=rate['KRW'], JPY=rate['JPY'])

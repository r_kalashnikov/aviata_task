import json
import requests
import xmltodict

from db.models import Rate
from datetime import datetime
from httpcore import Response
from typing import Dict

from settings import CURRENCY_URL


def build_flights_json(response_a: Response, response_b: Response) -> [dict]:
    answer_a = json.loads(response_a.text)
    answer_b = json.loads(response_b.text)

    return answer_a + answer_b


def convert(amount: float, from_currency: str, to_currency: str, rate: Rate) -> str:
    if from_currency == to_currency:
        return str(amount)

    elif from_currency == 'KZT':
        return str(round((amount / float(getattr(rate, to_currency))), 2))

    elif to_currency == 'KZT':
        return str(round((amount * float(getattr(rate, from_currency))), 2))

    else:
        kzt_amount = amount * float(getattr(rate, from_currency))
        request_amount = round((kzt_amount / float(getattr(rate, to_currency))), 2)
        return str(request_amount)


def sort_json_flight(flights_json: [dict]) -> [dict]:
    return sorted(flights_json, key=lambda flight: float(flight['price']['amount']))


async def add_price_and_sort_json_flight(flights_json: [dict], currency: str) -> [dict]:
    rate = await Rate.get(created_at=datetime.today())

    for flight in flights_json:
        flight_price = flight['pricing']['total']
        flight_currency = flight['pricing']['currency']

        flight['price'] = {}
        flight['price']['amount'] = convert(float(flight_price), flight_currency, currency, rate)
        flight['price']['currency'] = currency

    return sort_json_flight(flights_json)


def get_current_rate() -> Dict[str, float]:
    date = datetime.today().strftime('%d.%m.%Y')

    answer = requests.request('get', CURRENCY_URL + date)
    obj = xmltodict.parse(answer.text)
    json_rates = json.loads(json.dumps(obj))

    current_rate = {}

    for rate in json_rates['rates']['item']:
        current_rate[rate['title']] = float(rate['description'])

    return current_rate

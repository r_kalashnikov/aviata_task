import asyncio
import httpx
from httpx import Response

from helpers.parsers import build_flights_json
from settings import PROVIDER_A_URL, PROVIDER_B_URL, PROVIDER_A_TIMEOUT, PROVIDER_B_TIMEOUT


async def post_to_provider(provider_url: str, timeout: int) -> Response:
    async with httpx.AsyncClient() as client:
        res = await client.post(provider_url, timeout=timeout)
        return res


async def post_to_providers():
    result_a, result_b = asyncio.gather(post_to_provider(PROVIDER_A_URL, PROVIDER_A_TIMEOUT),
                                        post_to_provider(PROVIDER_B_URL, PROVIDER_B_TIMEOUT))

    return build_flights_json(result_a, result_b)

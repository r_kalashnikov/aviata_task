PROVIDER_A_URL: str = 'http://provider_a:8001/search'
PROVIDER_B_URL: str = 'http://provider_b:8002/search'
CURRENCY_URL: str = 'https://www.nationalbank.kz/rss/get_rates.cfm?fdate='

PROVIDER_A_TIMEOUT: int = 50
PROVIDER_B_TIMEOUT: int = 80

PROVIDER_A_TIME_TO_SLEEP: int = 10
PROVIDER_B_TIME_TO_SLEEP: int = 20
